import * as functions from "firebase-functions";
import {cert} from "./cred/cert";
import bodyParser = require("body-parser");
const admin = require('firebase-admin');
import * as express from "express";
admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
});

const app = express();
app.use(bodyParser.json());

const db = admin.firestore();
const deviceRefs = db.collection('appareils');

export async function postNewDevice(newDevice: any): Promise<any> {
    const addResult = await deviceRefs.add(newDevice);
    const createNewDevice = deviceRefs.doc(addResult.id);
    await createNewDevice.set({...newDevice, id: createNewDevice.id});

    return {...newDevice, id: createNewDevice.id};
}


app.post('/', async (req, res) => {
    try {
        const newDevice = req.body;
        const addResult = await postNewDevice(newDevice);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});

 export const do_create_device = functions
     .region("europe-west1")
     .runWith({memory: "128MB"})
     .https.onRequest(app);
