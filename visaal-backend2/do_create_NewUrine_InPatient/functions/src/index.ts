import * as functions from 'firebase-functions';
import * as  admin from "firebase-admin";
import * as express from 'express';
import * as bodyParser from 'body-parser';
import DocumentReference = admin.firestore.DocumentReference;
import {cert} from "./cred/cert";
import {firestore} from "firebase-admin/lib/firestore";
import DocumentSnapshot = firestore.DocumentSnapshot;
import FieldValue = firestore.FieldValue;

const app = express();
app.use(bodyParser.json());

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
});

const db = admin.firestore();
const patientRefs = db.collection('patients')
const refUrines = db.collection('urines');
export async function postNewUrineInPatient(newUrineData: any, patientId: string): Promise<any> {
    if (!patientId || !newUrineData){
        throw Error('patientId and newUrineData are missing')
    }
    const patientToFindRef = patientRefs.doc(patientId);
    const patientToFindSnap: DocumentSnapshot = await patientToFindRef.get();
    const patientData: PatientModel = patientToFindSnap.data() as PatientModel;
    const refPatient: DocumentReference = patientRefs.doc(patientId);
    const snapShotData: DocumentSnapshot = await refPatient.get();
    if (!snapShotData.exists) {
        return 'patient dose not existe';
    }

    const createDataUrineRef: DocumentReference = await refUrines.add(newUrineData);
    const createNewUrine: DocumentReference = refUrines
        .doc(createDataUrineRef.id);
    await createNewUrine.set({...newUrineData, id: createNewUrine.id});
    const createDataUrineInPatient: DocumentReference = refPatient
        .collection('urines')
        .doc(createDataUrineRef.id)
    const urinesId: any[] = [];

urinesId.push(createDataUrineRef.id)
    patientData.urines = urinesId;

    await refPatient.update({'urines': FieldValue.arrayUnion(createDataUrineRef.id)});
    await createDataUrineInPatient.set({ref: createDataUrineInPatient, id: createDataUrineInPatient.id, ...newUrineData}, {merge: true});
    return {...newUrineData, id: createDataUrineInPatient.id};
}
app.post('/:id', async (req, res) => {
    try {
        const newUrine = req.body;
        const patientId = req.params.id;
        const addResult = await postNewUrineInPatient(newUrine, patientId);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
export const do_create_NewUrine_InPatient = functions
    .region("europe-west1")
     .runWith({memory: "256MB"})
     .https
     .onRequest( app);
