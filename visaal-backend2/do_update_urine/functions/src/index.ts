import * as admin from "firebase-admin";
import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import {cert} from "./cred/cert";
import {firestore} from "firebase-admin/lib/firestore";
import DocumentReference = firestore.DocumentReference;
import DocumentSnapshot = firestore.DocumentSnapshot;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
});
const app = express();
app.use(cors());
app.use(bodyParser.json());
const db = admin.firestore();


const refPatients = db.collection('patients');

async function testIfPatientExistsById(patientId: string): Promise<DocumentReference> {
    const patientRef: DocumentReference = refPatients.doc(patientId);
    const snapPatientToFind: DocumentSnapshot = await patientRef.get();
    const patientToFind: PatientModel | undefined = snapPatientToFind.data() as PatientModel | undefined;
    if (!patientToFind) {
        throw new Error(`${patientId} does not exists`);
    }
    return patientRef;
}
export async function updatePatient(patientId: string, newPatient: PatientModel): Promise<PatientModel> {
    if (!newPatient || !patientId) {
        throw new Error(`patient data and patient id must be filled`);
    }

    const patientToPatchRef: DocumentReference = await testIfPatientExistsById(patientId);
    await patientToPatchRef.set(newPatient);
    return newPatient;
}
app.patch('/:id', async (req, res) => {
    try {
        const newUrine = req.body;
        const patientId = req.params.id;
        const addResult = await updatePatient(patientId, newUrine, );
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
 export const do_update_urine = functions
     .region("europe-west1")
     .runWith({memory: "256MB"})
     .https
     .onRequest( app);
