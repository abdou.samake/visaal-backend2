import * as functions from 'firebase-functions';
import * as  admin from "firebase-admin";
import * as express from 'express';
import * as bodyParser from 'body-parser';
import DocumentReference = admin.firestore.DocumentReference;
import {cert} from "./cred/cert";
import {firestore} from "firebase-admin/lib/firestore";
import DocumentSnapshot = firestore.DocumentSnapshot;
import {UrineModel} from "./model/urine";

const app = express();
app.use(bodyParser.json());

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
});

const db = admin.firestore();
const patientRefs = db.collection('patients')
const refUrines = db.collection('urines');

export async function postNewUrineInPatient(newUrineData: UrineModel, patientId: string): Promise<any> {
    if(!patientId || !newUrineData){
        throw Error('patientId and newUrineData are missing')
    }
    const patientToFindRef = patientRefs.doc(patientId);
    const patientToFindSnap: DocumentSnapshot = await patientToFindRef.get();
    const patientData: PatientModel = patientToFindSnap.data() as PatientModel;
    const refPatient: DocumentReference = patientRefs.doc(patientId);
    const snapShotData: DocumentSnapshot = await refPatient.get();
    if (!snapShotData.exists) {
        return 'patient dose not existe';
    }
    const createDataUrineRef: DocumentReference = await refUrines.add(newUrineData);
    const createDataUrineInPatient: DocumentReference = refPatient
        .collection('urines')
        .doc(createDataUrineRef.id);
    const urinesId: string[] = [];

    urinesId.push(createDataUrineRef.id);
    await refPatient.set({...patientData, urines: urinesId});
    await createDataUrineInPatient.set({ref: createDataUrineInPatient, id: createDataUrineRef.id, ...newUrineData});
    return {...newUrineData, id: createDataUrineRef.id};
}
app.post('/:id', async (req, res) => {
    try {
        const newUrine: UrineModel = req.body;
        const patientId = req.params.id;
        const addResult = await postNewUrineInPatient(newUrine, patientId);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
export const do_create_data_patients = functions
    .region("europe-west1")
    .runWith({memory: "256MB"})
    .https
    .onRequest( app);

