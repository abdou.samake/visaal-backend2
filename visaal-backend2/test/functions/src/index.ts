import * as functions from 'firebase-functions';

 export const test = functions
     .region("europe-west1")
     .runWith({memory: "128MB"})
     .https
     .onRequest((request, response) => {
   response.send("Hello from Visaal!");
 });
