import * as functions from 'firebase-functions';
import {firestore} from "firebase-admin/lib/firestore";
import DocumentReference = firestore.DocumentReference;
import DocumentData = firestore.DocumentData;
import * as express from "express";
const admin = require('firebase-admin');
import bodyParser = require("body-parser");
import {cert} from "./cred/cert";
admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
});

const app = express()
// app.use(cors());
app.use(bodyParser.json());

const db = admin.firestore();
const dataRefs = db.collection('urines')

export async function postNewUrine(newUrine: any): Promise<any> {
    if (!newUrine) {
        throw new Error(`new patient must be filled`);
    }
    const addResult: DocumentReference<DocumentData> = await dataRefs.add(newUrine);
    const createNewPatient: DocumentReference = dataRefs
        .doc(addResult.id);
    await createNewPatient.set({...newUrine, id: createNewPatient.id});

    return {...newUrine, id: createNewPatient.id};
}


app.post('/', async (req, res) => {
    try {
        const newUrine = req.body;
        const addResult = await postNewUrine(newUrine);
        return res.send(addResult);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
 export const do_create_urinePatient = functions
     .region("europe-west1")
     .runWith({memory: "128MB"})
     .https.onRequest(app);
