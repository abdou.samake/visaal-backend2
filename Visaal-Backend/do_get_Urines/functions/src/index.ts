import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
import {QuerySnapshot} from "@google-cloud/firestore";
import * as express from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import {cert} from "./cred/cert";
import {firestore} from "firebase-admin/lib/firestore";
import DocumentSnapshot = firestore.DocumentSnapshot;
import {UrineModel} from "./models/urine";

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://visaal-fc520.firebaseio.com",
});
const app = express();
app.use(cors());
app.use(bodyParser.json());
const db = admin.firestore();

const refUrines = db.collection('urines');

export async function getAllUrines(): Promise<any[]> {
    const urineQuerySnap: QuerySnapshot = await refUrines.get();
    const urines: any[] = [];
    urineQuerySnap.forEach(patientSnap => urines.push(patientSnap.data()));
    return urines;
}

export async function getUrineById(urineId: string): Promise<UrineModel> {
    if (!urineId) {
        throw new Error(`patientId required`);
    }
    const urineToFindRef = refUrines.doc(urineId);
    const urineToFindSnap: DocumentSnapshot = await urineToFindRef.get();
    if (urineToFindSnap.exists) {
        return urineToFindSnap.data() as UrineModel;
    } else {
        throw new Error('object does not exists');
    }
}
app.get('/:id', async (req, res) => {
    try {
        const urineId = req.params.id
        const urine = await getUrineById(urineId);
        return res.send(urine);
    } catch (e) {
        return res.status(500).send({error: 'erreur serveur :' + e.message});
    }
});
 export const getUrines = functions.https.onRequest(app);
